//
//  KeyCenter.swift
//  WhoopLook
//
//  Created by aybek can kaya on 12/07/16.
//  Copyright © 2016 aybek can kaya. All rights reserved.
//

import UIKit

class KeyCenter: NSObject {

    /*
     
     https://servicestudio.net/whoop/Services.svc/v1/geribildirim/getall
     
     
     [
     3:30
     ]
     https://servicestudio.net/whoop/Services.svc/v1/analiz/get
     
     
     [
     3:30
     ]
     https://servicestudio.net/whoop/Services.svc/v1/analiz/mesajcevap
     
     */
    
    internal static let baseURL:String = "https://servicestudio.net/whoop/Services.svc/"
    
    internal static let versionAPI:String = "v1"
    
    // urls
    internal static let urlGeribildirim:String = KeyCenter.baseURL+KeyCenter.versionAPI+"/geribildirim/getall"
    
     internal static let urlAnalizGet:String = KeyCenter.baseURL+KeyCenter.versionAPI+"/analiz/get"
    
     internal static let urlAnalizMesajCevap:String = KeyCenter.baseURL+KeyCenter.versionAPI+"/analiz/mesajcevap"
    
    
   
    
}

