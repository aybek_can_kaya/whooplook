//
//  MessageCell.swift
//  WhoopLook
//
//  Created by Can Özcan on 13.07.2016.
//  Copyright © 2016 aybek can kaya. All rights reserved.
//

import UIKit


class MessageCell: UITableViewCell {
    
    
    //var incomemessage:String = ""
    
    //var outgoingmessage:String = ""

    @IBOutlet weak var viewOuter: UIView!
    @IBOutlet weak var logomessage: UIImageView!
    @IBOutlet weak var labelmessage: UILabel!
    @IBOutlet weak var logoreponse: UIImageView!
    @IBOutlet weak var labelreponse: UILabel!

    @IBOutlet weak var viewSeperator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor=UIColor.whoopGrayColor
        self.viewSeperator.backgroundColor=UIColor.backgroundColor
        self.viewOuter.backgroundColor=UIColor.backgroundColorLightMessageCell

    }

    
    static func shouldShowResponse(textresponse:String)->Bool
    {
        let strResponse = textresponse.stringByReplacingOccurrencesOfString(" ", withString: "").lowercaseString
        if strResponse == "henüzyazılmamış."
        {
            return false
        }
        return true
    }
    
    
     func updateGUI()
     {
        // henüz yazılmamış
         let shouldShowResponse = MessageCell.shouldShowResponse(self.labelreponse.text!)
         if shouldShowResponse == false
         {
                self.labelreponse.alpha = 0
                self.logoreponse.alpha = 0
         }
        else
         {
            self.labelreponse.alpha = 1
            self.logoreponse.alpha = 1
         }
     }
    
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
