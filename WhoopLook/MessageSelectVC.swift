//
//  MessageSelectVC.swift
//  WhoopLook
//
//  Created by Can Özcan on 15.07.2016.
//  Copyright © 2016 aybek can kaya. All rights reserved.
//

import UIKit

class MessageSelectVC: BaseViewController , UITextViewDelegate{

    var selectedMessage:Message = Message()
    
    private var defaultContentSize:CGSize = CGSizeZero
    
    @IBOutlet weak var imViewCikintiMessageView: UIImageView!
    @IBOutlet weak var imViewCikintiReplyView: UIImageView!
    @IBOutlet weak var scrollTexts: UIScrollView!
    
    @IBOutlet weak var viewMessageArea: UIView!
    @IBOutlet weak var twMessage: UITextView!
    
    @IBOutlet weak var viewReplyArea: UIView!
    @IBOutlet weak var twReply: UITextView!
    
    
    
    
    
    override func viewDidLoad() {
        
        
        self.typeNavBar=NavigationBarTypeEnum.NavigationBarTypeWithBackButton
        self.titleNavBar=""
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        guiInit()
        updateGUI()

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let contentSize = self.twMessage.sizeThatFits(self.twMessage.bounds.size)
        var frame = self.twMessage.frame
        frame.size.height = contentSize.height
        self.viewMessageArea.frame = frame
        self.twMessage.frame = frame
        let aspectRatioTextViewConstraint = NSLayoutConstraint(item: self.twMessage, attribute: .Height, relatedBy: .Equal, toItem: self.twMessage, attribute: .Width, multiplier: self.twMessage.bounds.height / self.twMessage.bounds.width , constant: 1)
        let aspect2 = NSLayoutConstraint(item: self.viewMessageArea, attribute: .Height, relatedBy: .Equal, toItem: self.viewMessageArea, attribute: .Width, multiplier: self.twMessage.bounds.height / self.twMessage.bounds.width , constant: 1)
        self.twMessage.addConstraint(aspectRatioTextViewConstraint)
        self.viewMessageArea.addConstraint(aspect2)
        
        if twReply.text != "henüz yazılmamış."
        {
            let contentSizeReply = self.twReply.sizeThatFits(self.twReply.bounds.size)
            var frameReply = self.twReply.frame
            frameReply.size.height = contentSizeReply.height
            self.viewReplyArea.frame = frameReply
            self.twReply.frame = frameReply
            let aspectRatioTextReplyViewConstraint = NSLayoutConstraint(item: self.twReply, attribute: .Height, relatedBy: .Equal, toItem: self.twReply, attribute: .Width, multiplier: self.twReply.bounds.height / self.twReply.bounds.width , constant: 1)
            let aspectReply2 = NSLayoutConstraint(item: self.viewReplyArea, attribute: .Height, relatedBy: .Equal, toItem: self.viewReplyArea, attribute: .Width, multiplier: self.twReply.bounds.height / self.twReply.bounds.width , constant: 1)
            self.twReply.addConstraint(aspectRatioTextReplyViewConstraint)
            self.viewReplyArea.addConstraint(aspectReply2)
        }
        else
        {
            let contentSizeReply = self.twReply.sizeThatFits(self.twReply.bounds.size)
            var frameReply = self.twReply.frame
            frameReply.size.height = contentSizeReply.height
            self.viewReplyArea.frame = frame
            self.twReply.frame = frame
            let aspectRatioTextReplyViewConstraint = NSLayoutConstraint(item: self.twReply, attribute: .Height, relatedBy: .Equal, toItem: self.twReply, attribute: .Width, multiplier: self.twReply.bounds.height * 0 / self.twReply.bounds.width , constant: 1)
            let aspectReply2 = NSLayoutConstraint(item: self.viewReplyArea, attribute: .Height, relatedBy: .Equal, toItem: self.viewReplyArea, attribute: .Width, multiplier: self.twReply.bounds.height * 0 / self.twReply.bounds.width , constant: 1)
            self.twReply.addConstraint(aspectRatioTextReplyViewConstraint)
            self.viewReplyArea.addConstraint(aspectReply2)
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   /* func reInitUIPositions()
    {
        
        // message textview size
        let newSizeTwMessage:CGSize = self.twMessage.sizeThatFits(CGSizeMake(self.twMessage.frame.size.width, self.twMessage.frame.size.height*2))
        
        //print("new Size : \(newSizeTwMessage)")
        
        self.viewMessageArea.frame = CGRectMake(self.viewMessageArea.frame.origin.x , self.viewMessageArea.frame.origin.y, self.viewMessageArea.frame.size.width, newSizeTwMessage.height+8)
        
        self.twMessage.frame = CGRectMake(self.twMessage.frame.origin.x, self.twMessage.frame.origin.y, self.twMessage.frame.size.width, newSizeTwMessage.height)
        
        
        
        // cikinti ayarlari
        self.imViewCikintiMessageView.frame = CGRectMake(self.imViewCikintiMessageView.frame.origin.x, self.viewMessageArea.frame.origin.y + self.viewMessageArea.frame.size.height-4, self.imViewCikintiMessageView.frame.size.width, self.imViewCikintiMessageView.frame.size.height)
        self.imViewCikintiReplyView.frame = CGRectMake(self.imViewCikintiReplyView.frame.origin.x, self.viewReplyArea.frame.origin.y + self.viewReplyArea.frame.size.height-4, self.imViewCikintiReplyView.frame.size.width, self.imViewCikintiReplyView.frame.height)
        
        
        self.view.bringSubviewToFront(self.viewMessageArea)
        self.view.bringSubviewToFront(self.viewReplyArea)
        
        
        self.viewReplyArea.backgroundColor = UIColor.twReplyBackgroundColor
    }*/
    
    func guiInit(){
        self.twMessage.text = selectedMessage.message
        self.twReply.text = selectedMessage.reponse
        
       
        
        self.twMessage.font = UIFont.textViewFont()
        self.twReply.font = UIFont.textViewFont()
        
        
        self.viewMessageArea.backgroundColor = UIColor.backgroundColorDarkMessageCell
        self.viewReplyArea.backgroundColor = UIColor.twReplyBackgroundColor
        
        
    }
    
    
    
    func updateGUI() {
        if twReply.text == "henüz yazılmamış."
        {
            self.twReply.alpha = 0
            self.viewReplyArea.alpha = 0
            self.imViewCikintiReplyView.alpha = 0
        }
        else
        {
            self.twReply.alpha = 1
            self.viewReplyArea.alpha = 1
            self.imViewCikintiReplyView.alpha = 1

        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
