//
//  FeedbackCell.swift
//  WhoopLook
//
//  Created by Can Özcan on 15.07.2016.
//  Copyright © 2016 aybek can kaya. All rights reserved.
//

import UIKit

class FeedbackCell: UITableViewCell {

    @IBOutlet weak var viewOuter: UIView!
    
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var viewSeperator: UIView!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor=UIColor.whoopGrayColor
        self.viewSeperator.backgroundColor=UIColor.backgroundColor
        self.viewOuter.backgroundColor=UIColor.backgroundColorLightMessageCell
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
