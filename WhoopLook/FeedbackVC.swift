//
//  FeedbackVC.swift
//  WhoopLook
//
//  Created by Can Özcan on 15.07.2016.
//  Copyright © 2016 aybek can kaya. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner


class FeedbackVC: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableFeedback: UITableView!
    
    var feedbackArr:[Feedback] = []
    
    private var refreshControl:UIRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        self.typeNavBar = NavigationBarTypeEnum.NavigationBarTypeWithBackButton
        self.titleNavBar = "Feedbacks"
        
        super.viewDidLoad()
        
        self.tableFeedback.delegate = self
        self.tableFeedback.dataSource = self
        
        let cellnib:UINib = UINib(nibName: "FeedbackCell", bundle: nil)
        self.tableFeedback.registerNib(cellnib, forCellReuseIdentifier: "FeedbackCell")
        
        self.tableFeedback.backgroundColor = UIColor.clearColor()
      
        SwiftSpinner.show("Geri Bildirimler Yükleniyor Bebeğim...")
        self.getMessages()

        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.whiteColor()
        refreshControl.addTarget(self, action: #selector(self.getMessages), forControlEvents: UIControlEvents.ValueChanged)
        self.tableFeedback.addSubview(refreshControl)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedbackArr.count
    }
    //
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:FeedbackCell = tableView.dequeueReusableCellWithIdentifier("FeedbackCell") as! FeedbackCell
        
        
        cell.lblMessage.text = "-"
        cell.lblDate.text = "-"
        if self.feedbackArr.count > 0
        {
            cell.lblMessage?.text = self.feedbackArr[indexPath.row].feedback
            cell.lblDate?.text = NSDate.whoopDateFormat(self.feedbackArr[indexPath.row].date)
        }
       
        return cell
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
     
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let index:Int = indexPath.row
        let feedbackSelect:Feedback = feedbackArr[index]
        let msvc : FeedbackSelectVC = FeedbackSelectVC(nibName: "FeedbackSelectVC", bundle: nil)
        msvc.selectedFeedback = feedbackSelect
        self.navigationController?.pushViewController(msvc, animated: true)
        //msvc.reInitUIPositions()
    }
    
    func getMessages (){
        feedbackArr=[]
        
        Alamofire.request(.POST, KeyCenter.urlGeribildirim, parameters: ["Icerik":"Date"]).responseJSON { response in
            
            if response.result.isSuccess
            {
                if let theResult = response.result.value
                {
                    let theDctArray = theResult["data"] as! [NSDictionary]
                    //print(theDctArray)
                    for theDct in theDctArray {
                        let theFeedback = Feedback()
                        
                        theFeedback.feedback=theDct.objectForKey("Icerik") as! String
                        theFeedback.date=theDct.objectForKey("Date") as! UInt
                        
                        self.feedbackArr.append(theFeedback)
                    }
                    
                }
                
            }
            else if response.result.isFailure
            {
                print("error : \(response.result.error)")
            }
            self.tableFeedback.reloadData()
            SwiftSpinner.hide()
            self.refreshControl.endRefreshing()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
