//
//  FeedbackSelectVC.swift
//  WhoopLook
//
//  Created by Can Özcan on 15.07.2016.
//  Copyright © 2016 aybek can kaya. All rights reserved.
//

import UIKit

class FeedbackSelectVC: BaseViewController {

    var selectedFeedback:Feedback = Feedback()
    
    @IBOutlet weak var scrollText: UIScrollView!
    
    @IBOutlet weak var viewFeedbackArea: UIView!
    
    @IBOutlet weak var twFeedback: UITextView!
    
    @IBOutlet weak var imViewCikintiFeedbackArea: UIImageView!
    
    private var defaultContentSize:CGSize = CGSizeZero
    
    
    override func viewDidLoad() {
        
        self.typeNavBar=NavigationBarTypeEnum.NavigationBarTypeWithBackButton
        self.titleNavBar=""
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        guiInit()
        reInitUIPositions()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool){
        super.viewWillAppear(animated)
        let contentSize = self.twFeedback.sizeThatFits(self.twFeedback.bounds.size)
        var frame = self.twFeedback.frame
        frame.size.height = contentSize.height
        self.viewFeedbackArea.frame = frame
        self.twFeedback.frame = frame
        let aspectRatioTextViewConstraint = NSLayoutConstraint(item: self.twFeedback, attribute: .Height, relatedBy: .Equal, toItem: self.twFeedback, attribute: .Width, multiplier: self.twFeedback.bounds.height  / self.twFeedback.bounds.width , constant: 1)
        let aspect2 = NSLayoutConstraint(item: self.viewFeedbackArea, attribute: .Height, relatedBy: .Equal, toItem: self.viewFeedbackArea, attribute: .Width, multiplier: self.twFeedback.bounds.height  / self.twFeedback.bounds.width , constant: 1)
        self.twFeedback.addConstraint(aspectRatioTextViewConstraint)
        self.viewFeedbackArea.addConstraint(aspect2)
        
    }
    
    func guiInit(){
        
        self.twFeedback.text = selectedFeedback.feedback
        
        self.twFeedback.font = UIFont.textViewFont()
        
        self.viewFeedbackArea.backgroundColor = UIColor.backgroundColorDarkMessageCell
        //self.reInitUIPositions()
    }
    
    func reInitUIPositions() {
        
        
        //
        
        
        
        /*let newSizetwFeedback:CGSize = self.twFeedback.sizeThatFits(CGSizeMake(self.twFeedback.frame.size.width, self.twFeedback.frame.size.height*2))
        print(newSizetwFeedback.height)
        self.viewFeedbackArea.frame = CGRectMake(self.viewFeedbackArea.frame.origin.x, self.viewFeedbackArea.frame.origin.y, self.twFeedback.frame.size.width, newSizetwFeedback.height)
        
        self.twFeedback.frame = CGRectMake(self.twFeedback.frame.origin.x, self.twFeedback.frame.origin.y, self.twFeedback.frame.size.width, newSizetwFeedback.height)
        
        self.scrollText.contentSize = CGSizeMake(self.scrollText.contentSize.width, self.scrollText.contentSize.height)

        defaultContentSize = self.scrollText.contentSize
        
        self.imViewCikintiFeedbackArea.frame = CGRectMake(self.imViewCikintiFeedbackArea.frame.origin.x , self.viewFeedbackArea.frame.origin.y + self.viewFeedbackArea.frame.size.height - 4 , self.imViewCikintiFeedbackArea.frame.size.width, self.imViewCikintiFeedbackArea.frame.size.height)
        
        self.view.bringSubviewToFront(self.viewFeedbackArea)
        */
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
