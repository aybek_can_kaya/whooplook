//
//  IndexVC.swift
//  WhoopLook
//
//  Created by aybek can kaya on 12/07/16.
//  Copyright © 2016 aybek can kaya. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner

class IndexVC: BaseViewController {

    
    var kullaniciData : [String] = []
    
    var mesajData : [String] = []
    
    var cevapData : [String] = []
    
    @IBOutlet weak var lblBugunAtilanMesaj: UILabel!
  
    @IBOutlet weak var lblBugunAtilanCevap: UILabel!
    
    @IBOutlet weak var lblBugunKayitOlanKisi: UILabel!
    
    @IBOutlet weak var lblToplamMesaj: UILabel!
    
    @IBOutlet weak var lblToplamCevap: UILabel!
    
    @IBOutlet weak var lblToplamKullanici: UILabel!
    
    @IBOutlet weak var lblMesaj: UILabel!
    
    @IBOutlet weak var lblCevap: UILabel!
    

    @IBOutlet weak var lblKullanici: UILabel!
    
    @IBOutlet weak var lblMesajVeri: UILabel!
    
    @IBOutlet weak var lblCevapVeri: UILabel!
    
    @IBOutlet weak var lblKullaniciVeri: UILabel!
    
    @IBOutlet weak var scTodayOrTotal: UISegmentedControl!
    
    /**
       network call timer. At each tick this forces update the gui by calling "networRequest()"
     */
    private var timerNetwork:NSTimer? // optional -> may become nil
    
    
    override func viewWillAppear(animated: Bool) {
       super.viewWillAppear(animated)
       
        // call network request each 3 seconds
        self.timerNetwork = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: #selector(networkRequest), userInfo: nil, repeats: true)
    }
    
    override func viewDidLoad() {
        self.typeNavBar=NavigationBarTypeEnum.NavigationBarTypeIndexPage
        self.titleNavBar="Index"
        
        super.viewDidLoad()

        // Do any additional setup after loading the view
        
        guiInit()
        SwiftSpinner.show("Veriler Yükleniyor Bebeğim...")
        networkRequest() // init call 
        
      }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        // dismiss the timer for avoiding possible CRASH 
        if self.timerNetwork != nil
        {
            self.timerNetwork?.invalidate()
            self.timerNetwork = nil
        }
    }
    
    
       func guiInit()
       {
        
        //Segmented Control Init
        self.scTodayOrTotal.backgroundColor = UIColor.backgroundColor
        self.scTodayOrTotal.tintColor = UIColor.whoopGreenColor
        
        
        self.lblMesaj.font = UIFont.whoopLookBoldTextFont()
        self.lblMesajVeri.font =  UIFont.whoopLookTextFont()
        self.lblCevap.font =  UIFont.whoopLookBoldTextFont()
        self.lblCevapVeri.font =  UIFont.whoopLookTextFont()
        self.lblKullanici.font =  UIFont.whoopLookBoldTextFont()
        self.lblKullaniciVeri.font =  UIFont.whoopLookTextFont()
        
        self.lblMesaj.text="Toplam Mesaj Sayısı:"
        self.lblCevap.text="Toplam Cevap Sayısı:"
        self.lblKullanici.text="Toplam Kullanıcı Sayısı:"
        self.lblMesajVeri.text = "-"
        self.lblCevapVeri.text = "-"
        self.lblKullaniciVeri.text = "-"

        
       }
    
    
    
    
    func networkRequest()
    {
       
        
        Alamofire.request(.POST, KeyCenter.urlAnalizGet , parameters: [:]).responseJSON { response in
            
            if response.result.isSuccess
            {
                if let theResult = response.result.value
                {
                    let theDct = theResult["data"] as! NSDictionary
                    print("the result : \(theDct)")

                    self.kullaniciData = []
                    self.cevapData = []
                    self.mesajData = []
                    
                    self.kullaniciData.append((theDct.objectForKey("toplamKullanici") as? String)!)
                    self.kullaniciData.append((theDct.objectForKey("bugunKayitOlan") as? String)!)
                    self.mesajData.append((theDct.objectForKey("toplamMesaj") as? String)!)
                    self.mesajData.append((theDct.objectForKey("bugunAtilanMesaj") as? String)!)
                    self.cevapData.append((theDct.objectForKey("toplamCevap") as? String)!)
                    self.cevapData.append((theDct.objectForKey("bugunAtilanCevap") as? String)!)
                    
                    /*
                    self.lblMesajVeri.text = self.mesajData[0]
                    self.lblCevapVeri.text = self.cevapData[0]
                    self.lblKullaniciVeri.text = self.kullaniciData[0]
                    */
                    self.FuncTodayOrTomorrow(self.scTodayOrTotal)
                    
                }

            }
            else if response.result.isFailure
            {
                print("error : \(response.result.error)")
            }
            SwiftSpinner.hide()
                
           
            
        }
        
    }
    
    
    //Segmented Control Function
    @IBAction func FuncTodayOrTomorrow(sender: UISegmentedControl) {
      
        switch scTodayOrTotal.selectedSegmentIndex {
        //Today's Data
        case 1:
            
            self.lblMesaj.text="Bugün Atılan Mesaj Sayısı:"
            self.lblCevap.text="Bugün Atılan Cevap Sayısı:"
            self.lblKullanici.text="Bugün Kayıt Olan Kullanıcı Sayısı:"
            /*
            self.lblMesajVeri.text = "-"
            self.lblCevapVeri.text = "-"
            self.lblKullaniciVeri.text = "-" */
            break
        //Total's Data
        default:
            
            self.lblMesaj.text="Toplam Mesaj Sayısı:"
            self.lblCevap.text="Toplam Cevap Sayısı:"
            self.lblKullanici.text="Toplam Kullanıcı Sayısı:"
            /*
            self.lblMesajVeri.text = "-"
            self.lblCevapVeri.text = "-"
            self.lblKullaniciVeri.text = "-" */
            break
        }
        
        self.lblMesajVeri.text = self.mesajData[scTodayOrTotal.selectedSegmentIndex]
        self.lblCevapVeri.text = self.cevapData[scTodayOrTotal.selectedSegmentIndex]
        self.lblKullaniciVeri.text = self.kullaniciData[scTodayOrTotal.selectedSegmentIndex]
        
        print("did Select ")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
