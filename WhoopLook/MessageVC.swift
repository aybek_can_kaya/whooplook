//
//  MessageVC.swift
//  WhoopLook
//
//  Created by Can Özcan on 12.07.2016.
//  Copyright © 2016 aybek can kaya. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner


class MessageVC: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableMessage: UITableView!
    
    var messagesArr:[Message] = []
    
    
    
    override func viewDidLoad() {
        self.typeNavBar=NavigationBarTypeEnum.NavigationBarTypeWithBackButton
        self.titleNavBar="Messages"
        super.viewDidLoad()
        self.tableMessage.delegate = self
        self.tableMessage.dataSource = self
        
        let cellNib:UINib = UINib(nibName: "MessageCell", bundle: nil)
        self.tableMessage.registerNib(cellNib, forCellReuseIdentifier: "MessageCell")
        
        self.tableMessage.backgroundColor = UIColor.clearColor()
        SwiftSpinner.show("Mesajlar Yükleniyor Bebeğim...")
        self.getMessages()
        //
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:MessageCell = tableView.dequeueReusableCellWithIdentifier("MessageCell") as! MessageCell
        
        cell.labelmessage?.text = self.messagesArr[indexPath.row].message
        cell.labelreponse?.text = self.messagesArr[indexPath.row].reponse
    
        cell.updateGUI()


        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let message:Message = self.messagesArr[indexPath.row]
        
        if MessageCell.shouldShowResponse(message.reponse) == false
        {
            return 60
        }
        
        return 120
    }
        
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let index:Int = indexPath.row
        let messageSelect:Message = messagesArr[index]
        let msvc : MessageSelectVC = MessageSelectVC(nibName: "MessageSelectVC", bundle: nil)
        msvc.selectedMessage=messageSelect
        //msvc.reInitUIPositions()
        self.navigationController?.pushViewController(msvc, animated: true)
        //msvc.updateGUI()
        
        
    }
    
    func getMessages (){
        messagesArr=[]
        
        Alamofire.request(.POST, KeyCenter.urlAnalizMesajCevap, parameters: ["Mesaj":"Cevap"]).responseJSON { response in
            
            if response.result.isSuccess
            {
                if let theResult = response.result.value
                {
                    let theDctArray = theResult["data"] as! [NSDictionary]
                    //print(theDctArray)
                    for theDct in theDctArray {
                        let theMessage = Message()
                        
                        theMessage.message=theDct.objectForKey("Mesaj") as! String
                        theMessage.reponse=theDct.objectForKey("Cevap") as! String
                        
                        self.messagesArr.append(theMessage)
                    }
                    
                }
                
            }
            else if response.result.isFailure
            {
                print("error : \(response.result.error)")
            }
            self.tableMessage.reloadData()
            SwiftSpinner.hide()
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
