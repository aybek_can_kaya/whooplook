//
//  TRFont.swift
//  Tripografy
//
//  Created by Gökhan Gültekin on 10/02/16.
//  Copyright © 2016 Peakode. All rights reserved.
//

import UIKit

enum FontStyle {
    case FontStyleBlack
    case FontStyleBlackItalic
     case FontStyleBold
    case FontStyleBoldItalic
     case FontStyleHairline
    case FontStyleHairlineItalic
    case FontStyleLight
    case FontStyleLightItalic
    case FontStyleMedium
    case FontStyleMediumItalic
    case FontStyleRegular
    case FontStylePlaceholder
}

extension UIFont {
    
    static func fontWithSize(fontSize: CGFloat, style: FontStyle) -> UIFont {
        
        var suffix:String?
        
        if style == FontStyle.FontStyleBlack {
            suffix = "-Black"
        } else if style == FontStyle.FontStyleBlackItalic {
            suffix = "-BlackItalic"
        } else if style == FontStyle.FontStyleBold {
            suffix = "-Bold"
        } else if style == FontStyle.FontStyleBoldItalic {
            suffix = "-BoldItalic"
        } else if style == FontStyle.FontStyleMedium {
            suffix = "-Regular"
        } else if style == FontStyle.FontStyleMediumItalic {
            suffix = "-Italic"
        } else if style == FontStyle.FontStyleLight {
            suffix = "-Light"
        } else if style == FontStyle.FontStyleLightItalic {
            suffix = "-LightItalic"
        } else if style == FontStyle.FontStyleHairline {
            suffix = "-Hairline"
        }else if style == FontStyle.FontStyleHairlineItalic {
            suffix = "-HairlineItalic"
        }  else if style == FontStyle.FontStyleRegular {
            suffix = "-Regular"
        }
        else if style == FontStyle.FontStylePlaceholder
        {
             suffix = "-Oblique"
            let fontName = "Avenir"+suffix!
            let font = UIFont(name: fontName, size: fontSize)
            return font!
        }
        
        let fontName = "Lato"+suffix!
        
        let font = UIFont(name: fontName, size: fontSize)
        
        return font!
    }
    
}

// ========================================
// FONT SIZE INFO:
// ========================================
/*
Button
Text - Ubuntu Medium - 14 px
Size - Width 270 px, Height 35 px, Radius 3 px, x 25px

Text Field
Text - Ubuntu Regular - 13 px
Size - Width 270 px, Height 35 px, Radius 3 px, x 25px(edited)
*/



// ========================================
// MARK: APPLICATION FONTS
// ========================================


extension UIFont{
    
    static func navigationBarFont()->UIFont{
        return UIFont.fontWithSize(16, style: FontStyle.FontStyleMedium)
    }
    
    
    static func textViewFont()->UIFont
    {
         return UIFont.fontWithSize(16, style: FontStyle.FontStyleRegular)
    }
    
    static func textviewPlaceholderFont()->UIFont
    {
        return UIFont.fontWithSize(16, style: FontStyle.FontStylePlaceholder)
    }
    
    
    static func whoopLookBoldTextFont()->UIFont
    {
        return UIFont.fontWithSize(16, style: FontStyle.FontStyleBold)
    }
    
    static func whoopLookTextFont()->UIFont
    {
        return UIFont.fontWithSize(16, style: FontStyle.FontStyleLight)
    }
    
    
}




