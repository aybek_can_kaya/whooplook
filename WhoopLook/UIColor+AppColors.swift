

import Foundation
import UIKit

extension UIColor {
    
    @nonobjc static let backgroundColor = UIColor(red: 71/255, green: 71/255, blue: 71/255, alpha: 1)
    
    @nonobjc static let whoopRedColor = UIColor(red: 248/255, green: 80/255, blue: 95/255, alpha: 1)
    
    @nonobjc static let whoopGreenColor = UIColor(red: 70/255, green: 213/255, blue: 147/255, alpha: 1)
    
    @nonobjc static let whoopGrayColor = UIColor(red: 110/255, green: 110/255, blue: 110/255, alpha: 1)

     @nonobjc static let backgroundColorDark = UIColor(red: 54/255, green: 54/255, blue: 54/255, alpha: 1)
    
    @nonobjc static let backgroundColorLight = UIColor(red: 87/255, green: 87/255, blue: 87/255, alpha: 1)
    
    @nonobjc static let backgroundColorDarkMessageCell = UIColor(red: 83/255, green: 83/255, blue: 83/255, alpha: 1)
    
    @nonobjc static let backgroundColorLightMessageCell = UIColor(red: 67/255, green: 67/255, blue: 67/255, alpha: 1)
    
    
    
     @nonobjc static let textviewPlaceholderColor = UIColor(red: 138/255, green: 138/255, blue: 138/255, alpha: 1)
   
    @nonobjc static let textviewColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
    
    @nonobjc static let twReplyBackgroundColor = UIColor(red: 108/255, green: 108/255, blue: 108/255, alpha: 1)
    

    
     @nonobjc static let toastAlertBackgroundColorSuccess = UIColor(red: 70/255, green: 213/255, blue: 147/255, alpha: 1)
    
    @nonobjc static let toastAlertBackgroundColorError = UIColor(red: 248/255, green: 80/255, blue: 95/255, alpha: 1)
    
   
    
    
    /*
     UIColor(red: 0.0, green: 0.0, blue: 0.0980392, alpha: 0.22)
     
    @nonobjc static let navigationBarTintColor = UIColor(red: 147/255, green: 209/255, blue: 223/255, alpha: 1)
    
    
    @nonobjc static let navigationBarTextColor = UIColor.whiteColor()
    @nonobjc static let hashtagColor = UIColor(red: 254/255, green: 156/255, blue: 96/255, alpha: 1)
    @nonobjc static let tabBarHighlight = UIColor.hashtagColor
    
    @nonobjc static let detailedActivityTextColor = UIColor(red: 158/255, green: 151/255, blue: 146/255, alpha: 1) //brown
    @nonobjc static let lightBrownFontColor = UIColor(red: 220/255, green: 210/255, blue: 201/255, alpha: 1)//light brown
    
    @nonobjc static let signupButtonColor = UIColor(red: 48/255, green: 180/255, blue: 176/255, alpha: 1)
    
    @nonobjc static let doneButtonColor = UIColor(red: 37/255, green: 180/255, blue: 177/255, alpha: 1)
    
    @nonobjc static let nearbyCityHighlight = UIColor(red: 111/255, green: 135/255, blue: 140/255, alpha: 1)
    
    @nonobjc static let dropDownBtnColor = UIColor(red: 241/255, green: 237/255, blue: 236/255, alpha: 1) //light gray // tableView background
    
    @nonobjc static let filterBtnHighlight = UIColor(red: 158/255, green: 151/255, blue: 146/255, alpha: 1)//dark brown
    
    
     @nonobjc static let tableViewSeperatorColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
    
    
    @nonobjc static let activityIndicatorColorNavBar = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) //white
    
    
    @nonobjc static let activityIndicatorColorTopPage = UIColor(red: 55/255, green: 55/255, blue: 55/255, alpha: 1) //dark gray
    
    // 252 , 125 ,112
    @nonobjc static let alertviewBGColor = UIColor(red: 252/255, green: 125/255, blue: 112/255, alpha: 1) //mix of red-white
    
    // 43 178 174
    @nonobjc static let socialAccountsTextColor = UIColor(red: 43/255, green: 178/255, blue: 174/255, alpha: 1) // turkuaz
    
    
    // 55 55 55
    @nonobjc static let activityViewBGColor = UIColor(red: 25/255, green: 25/255, blue: 25/255, alpha: 0.8) // black
    
    
    
    // 100 , 197 , 223
    @nonobjc static let backgroundColorTurkuaz = UIColor(red: 100/255, green: 197/255, blue: 223/255, alpha: 1) // sign up bg view 
    */
    
}