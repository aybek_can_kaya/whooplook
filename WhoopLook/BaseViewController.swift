//
//  BaseViewController.swift
//  WhoopLook
//
//  Created by aybek can kaya on 12/07/16.
//  Copyright © 2016 aybek can kaya. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    enum NavigationBarTypeEnum:Int
    {
        /**
         no buttons on navigation bar
         */
        case NavigationBarTypeNoButtons = 0
         /*
            with back buttons */
        
        
        case NavigationBarTypeIndexPage
        
        case NavigationBarTypeWithBackButton
        
    }
    
    var titleNavBar:String = ""
    var typeNavBar:NavigationBarTypeEnum = NavigationBarTypeEnum.NavigationBarTypeNoButtons
    var selectorNameforback:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        self.view.backgroundColor = UIColor.backgroundColor
        self.setupNavigationBar()
      
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BaseViewController {
    
    func setupNavigationBar(){
        
        self.navigationController?.navigationBar.translucent = false
        if(self.navigationController == nil){
            return
        }
        
        // Setup
        
        self.navigationController?.navigationBar.barTintColor = UIColor.backgroundColorLightMessageCell
        
        let font:UIFont = UIFont.navigationBarFont()
        let titleDict: [String: AnyObject] = [NSForegroundColorAttributeName: UIColor.whoopGreenColor, NSFontAttributeName: font]
        
        self.navigationController!.navigationBar.titleTextAttributes = titleDict
        self.title = titleNavBar
        
        switch typeNavBar {
        case .NavigationBarTypeWithBackButton:
            let viewBack = self.navigationItemView(fromButtonImageName: "back_btn", selectorName: "backOnTap",isLeftButton: true)
            let leftBarBtn:UIBarButtonItem = UIBarButtonItem(customView: viewBack)
            self.navigationItem.leftBarButtonItems = [leftBarBtn]
            break
        case .NavigationBarTypeIndexPage:
            let viewMessage = self.navigationItemView(fromButtonImageName: "mail_btn", selectorName: "clickMessage",isLeftButton: true)
            let leftBarBtn:UIBarButtonItem = UIBarButtonItem(customView: viewMessage)
            self.navigationItem.leftBarButtonItems = [leftBarBtn]
            let viewFeedback = self.navigationItemView(fromButtonImageName: "settings_btn", selectorName: "clickFeedback",isLeftButton: false)
            let rightBarBtn:UIBarButtonItem = UIBarButtonItem(customView: viewFeedback)
            self.navigationItem.rightBarButtonItems = [rightBarBtn]
            break
        //No buttons
        default:
            self.navigationItemView(fromButtonImageName: "Mesajlar", selectorName: "clickMessage", isLeftButton: false)
            self.navigationItemView(fromButtonImageName: "Geri Bildirimler", selectorName: "clickFeedback", isLeftButton: true)
            break
        }
    }
    
    /**
     Custom navigation bar Item with usable button component
     
     - parameter btnName: image path for button
     - parameter slName:  selector that is fired after button tap
     
     - returns: UIView with usable button .
     */
    
    private func navigationItemView(fromButtonImageName btnName:String , selectorName slName:String , isLeftButton:Bool )->UIView
    {
        let viewButton = navigationItemEmptyView()
        
        let btn:UIButton = UIButton()
        
        let margin:CGFloat = CGFloat(16)
        
        
        if(isLeftButton)
        {
            btn.frame = CGRectMake(-1*margin, 0, 50, 50)
        }
        else
        {
            btn.frame = CGRectMake(margin, 0, 50, 50)
        }
        
        
        btn.setImage(UIImage(named: btnName), forState: UIControlState.Normal)
        btn.setImage(UIImage(named: btnName), forState: UIControlState.Highlighted)
        
        
        
        
        
        btn.addTarget(self, action: Selector(slName), forControlEvents: UIControlEvents.TouchUpInside)
        
        viewButton.addSubview(btn)
        
        
        
        return viewButton
        
    }
    
    /**
     creates empty view for UIBarButton
     
     - returns: empty clear colored view for Bar Button (Navigation Bar)
     */
    private func navigationItemEmptyView()->UIView
    {
        let viewButton:UIView = UIView()
        viewButton.frame = CGRectMake(0, 0, 50, 50)
        viewButton.backgroundColor = UIColor.clearColor()
        
        return viewButton
    }
    
    //MARK: Selectors
    
    func clickHomePage() {
        self.navigationController?.pushViewController(IndexVC(), animated: true)
    }
    
    func clickMessage() {
        self.navigationController?.pushViewController(MessageVC(), animated: true)
    }
    
    func clickFeedback() {
        self.navigationController?.pushViewController(FeedbackVC(), animated: true)
    }
    func backOnTap(){
        self.navigationController?.popViewControllerAnimated(true)
    }

}
